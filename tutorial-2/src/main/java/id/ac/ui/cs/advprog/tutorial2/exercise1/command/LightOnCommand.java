package id.ac.ui.cs.advprog.tutorial2.exercise1.command;

import id.ac.ui.cs.advprog.tutorial2.exercise1.receiver.Light;

public class LightOnCommand implements Command {

    private Light light;

    public LightOnCommand(Light light) {
        this.light = light;
    }

    @Override
   public void execute() {
        this.light.on();
        // TODO Complete me!
    }

    @Override
    public void undo() {
        this.light.off();
        // TODO Complete me!
    }
}
