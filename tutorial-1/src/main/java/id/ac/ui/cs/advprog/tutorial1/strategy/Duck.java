package id.ac.ui.cs.advprog.tutorial1.strategy;

public abstract class Duck {

    private FlyBehavior flyBehavior;
    private QuackBehavior quackBehavior;

    public void performFly() {
        flyBehavior.fly();
    }

    public void performQuack() {
        quackBehavior.quack();
    }

    public abstract void display();

    public void setFlyBehavior(FlyBehavior realFlyBehavior) {
        this.flyBehavior = realFlyBehavior;

    }

    public void setQuackBehavior(QuackBehavior realBehavior) {
        this.quackBehavior = realBehavior;
    }

    public void swim() {
        System.out.println("swim");
    }
}
